## [1.2.0](https://gitlab.com/kilianpaquier/hugot/compare/v1.1.0...v1.2.0) (2024-01-14)


### Code Refactoring

* allow disabling of footer and put footer into its own section ([30803da](https://gitlab.com/kilianpaquier/hugot/commit/30803dab63edb3c412d4f586ecfa61f2aa5c6f16))

## [1.1.0](https://gitlab.com/kilianpaquier/hugot/compare/v1.0.2...v1.1.0) (2024-01-14)


### Features

* add bottom copyright ([62e948b](https://gitlab.com/kilianpaquier/hugot/commit/62e948b495c4634b3d3441fe35f72e98be9ad8d7))

## [1.0.2](https://gitlab.com/kilianpaquier/hugot/compare/v1.0.1...v1.0.2) (2023-11-19)


### Chores

* update go version and fix cicd project name ([303ffab](https://gitlab.com/kilianpaquier/hugot/commit/303ffabf66dd6277e65fc0d86e4057c8519eee9f))

## [1.0.1](https://gitlab.com/kilianpaquier/hugot/compare/v1.0.0...v1.0.1) (2023-07-25)


### Bug Fixes

* **ci:** remove bad go pipeline import ([e7cb5cf](https://gitlab.com/kilianpaquier/hugot/commit/e7cb5cf19e5f9d44a0e795f4c8d4eaf0edd7dad5))


### Chores

* **ci:** removed publish after v4 cicd templates ([205106b](https://gitlab.com/kilianpaquier/hugot/commit/205106bd758c27413803880ea7850f75d7c6d6a3))
* **ci:** update with latest cicd templates ([cbed5b3](https://gitlab.com/kilianpaquier/hugot/commit/cbed5b3bb860bbd1b588ace0c5ce27ec9fe41128))
* **license:** update ([a31c07e](https://gitlab.com/kilianpaquier/hugot/commit/a31c07e348aa819265657086873010568c3f3df0))
